
import FormSignup from './auth/Form/FormSignup';
import  useDocumentTitle  from '../hooks/useDocumentTitle'
import FormSignin from './auth/Form/FormSignin';
import { useNavigate } from 'react-router-dom';


export function HomeController() {

  useDocumentTitle("Sign in");
  

  const navigate = useNavigate();
  const navigateSingUp = async () =>{ navigate('/signup');}


  return (
    <>

      <br/><br/><br/><br/>
      <div className="container">
          <div className="row">
              <div className="col-sm-6 mx-auto">
                  <h4 className="h4 mb-3 fw-normal">Welcome</h4>
                  <h1 className="h2 mb-3 fw-normal">Please sign in</h1>
                      <FormSignin />
                  <div className="text-center col-12">
                    <a href="#" className="link-primary" onClick={navigateSingUp}>Sign up</a>
                  </div>
              </div>
          </div>
      </div>

    </>
 
  );

}

