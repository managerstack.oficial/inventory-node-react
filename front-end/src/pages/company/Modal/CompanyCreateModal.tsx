import { createRef, useRef, useState } from "react";
import { FaPlusCircle } from "react-icons/fa";
import { useDispatch } from "react-redux";
import ModalWrapper from "../../../components/modal/modalWrapper";
import { companyCreate, companyList } from "../../../reducers/company/companyServices";
import { AppThunkDispatch } from "../../../store/store";
import { CompanyAttrs } from "../../../utils/interfaces/company/companyInterfaces";
import {FormCompany} from "../Form/FormCompany";

export const CompanyAddModal = () =>{

  const dispatch = useDispatch<AppThunkDispatch>();
  const [modalShow, setModalShow] = useState(false);
  const formRef = useRef<HTMLButtonElement>(null);

  const callSubmit = () => {
      if (formRef.current != null)
        formRef.current.click();
        //current.handleSubmit(values, actions);
  }

  const companyToggleHandler = () =>{
    setModalShow(!modalShow);
  }

  const companyActionHandler = async (values: CompanyAttrs) =>{
    const {name, address, nit, phone } = values;
    await dispatch(companyCreate({name, address, nit, phone}));
    await dispatch(companyList());
  }
  //onSubmitHandler={companyActionHandler}
  return (
      <>
        <ModalWrapper 
          title="Create a new Company"
          show={modalShow} 
          handleClose={companyToggleHandler} 
          handleShow={callSubmit} >
            
            <FormCompany 
              ref={formRef} 
              onSubmitHandler={companyActionHandler}
              name="" 
              address=""
              nit=""
              phone=""
              status="success" ></FormCompany>
        
        </ModalWrapper>

        <button type="button" className="btn btn-success float-end" onClick={()=>{companyToggleHandler()}} >
                  Add <FaPlusCircle className="icon-event" />
        </button>
      </>
  )


}