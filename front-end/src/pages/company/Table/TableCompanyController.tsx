import TableCompany from "./TableCompany";
import { useDispatch, useSelector } from 'react-redux';
import { CompanyStateAttrs } from "../../../utils/interfaces/company/companyInterfaces";
import { AppThunkDispatch } from '../../../store/store';
import { useEffect } from "react";
import { companyList } from "../../../reducers/company/companyServices";
import { CompanyAddModal } from "../Modal/CompanyCreateModal";


function ControllerTableCompany() {

  const {status, error, companies } = useSelector((state: { company: CompanyStateAttrs }) => state.company);
  const dispatch = useDispatch<AppThunkDispatch>();

  useEffect(() => {
    dispatch(companyList());
  }, [dispatch]);


  return (
    <>
      <div className="row">
        <div className="col-lg-4 col-md-12">
          <h1>Companies</h1>
        </div>

        <div className="col-lg-4 col-md-6">

        </div>

        <div className="col-lg-4 col-md-6">
        <CompanyAddModal></CompanyAddModal>
        </div>
      </div>

      {(status == 'failed') && <div className="alert alert-warning" role="alert">
        {error}
      </div>}

      {(status == 'succeeded' && companies.length > 0) && <TableCompany companies={companies}></TableCompany>}

      {(status == 'succeeded' && companies.length == 0) && <div className="alert alert-danger" role="alert">
        This company currently has no products, please proceed to a registrar.
      </div>}

      {(status == 'idle' || status == 'loading') && <div className="placeholder-glow text-center">
        <span className="placeholder col-12 big"></span>
        <p className="mt-1">...Loading</p>
      </div>}
    </>
  );
}

export default ControllerTableCompany;