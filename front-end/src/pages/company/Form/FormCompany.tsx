import React, { forwardRef } from 'react';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';


const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/


interface MyFormValues {
    name: string;
    address: string;
    nit: string;
    phone: string;
}

export const FormCompany = forwardRef((props: any, ref: any) => {
    const {status, onSubmitHandler, nit, name, address, phone, description} = props;

    const initialValues: MyFormValues = { 
        name: name, 
        address: address, 
        nit: nit,
        phone:phone,
    };

    const handleSubmit = (values: any, actions: any) => {        
        onSubmitHandler(values);
        actions.setSubmitting(false);
    }
    
    const digitsOnly = (value: string) => /^\d+$/.test(value)
    
    const validationSchema = Yup.object({
        name: Yup.string()
            .min(6, 'Min. 6 characters')
            .max(50, 'Max. 50 characters')
            .required('Email is required'),

        address: Yup.string()
            .min(5, 'Min. 5 characters')
            .max(100, 'Max. 100 characters')
            .required("Required"),

        nit: Yup
            .string()
            .min(9, 'Min. 9 characters')
            .max(9, 'Max. 9 characters')
            .required("Required")
            .test('Digits only', 'The field should have digits only', digitsOnly),

        phone: Yup.string()
            .required()
            .matches(phoneRegExp, 'Phone number is not valid')
            .min(10, 'Min. 10 characters')
            .max(10, 'Max. 10 characters')
            .test('Digits only', 'The field should have digits only', digitsOnly)
    })

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
        >
            {(formik) => (
                <Form >
                    <fieldset disabled={status == 'loading'}>
                        <div className="form-group mt-1">
                            
                            <label htmlFor="email">
                                Name
                            </label>
                            
                            <Field 
                                name="name" 
                                className={(formik.touched.name && formik.errors.name) ? 'form-control is-invalid' : 'form-control'} 
                                type="text" />
                            
                            {formik.touched.name && formik.errors.name ? 
                            (<div className="invalid-feedback">{formik.errors.name}</div> ) : null}
                        </div>


                        <div className="form-group mt-1">
                            
                            <label htmlFor="address">
                                Address
                            </label>
                            
                            <Field 
                                name="address" 
                                className={(formik.touched.address && formik.errors.address) ? 'form-control is-invalid' : 'form-control'} 
                                type="text" />
                            
                            {formik.touched.address && formik.errors.address ? 
                            (<div className="invalid-feedback">{formik.errors.address}</div> ) : null}
                        </div>

                        <div className="form-group mt-1">
                            
                            <label htmlFor="nit">
                                NIT
                            </label>
                            
                            <Field 
                                name="nit" 
                                className={(formik.touched.nit && formik.errors.nit) ? 'form-control is-invalid' : 'form-control'} 
                                type="text" />
                            
                            {formik.touched.nit && formik.errors.nit ? 
                            (<div className="invalid-feedback">{formik.errors.nit}</div> ) : null}
                        </div>

                        <div className="form-group mt-1">
                            
                            <label htmlFor="phone">
                                Phone
                            </label>
                            
                            <Field 
                                name="phone" 
                                className={(formik.touched.phone && formik.errors.phone) ? 'form-control is-invalid' : 'form-control'} 
                                type="phone" />
                            
                            {formik.touched.phone && formik.errors.phone ? 
                            (<div className="invalid-feedback">{formik.errors.phone}</div> ) : null}
                        </div>
                        <button type='submit' className="no-visible" ref={ref}>as</button>
                    </fieldset>        
                </Form>
            )}
        </Formik>
    );
})