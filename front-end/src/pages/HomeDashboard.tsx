
import  useDocumentTitle  from '../hooks/useDocumentTitle'
import ControllerTableCompany from './company/Table/TableCompanyController';

export function HomeDashboard() {

  useDocumentTitle("Dashboard");

  return (
    <>

      <ControllerTableCompany></ControllerTableCompany>
      
    </>
  );

}

