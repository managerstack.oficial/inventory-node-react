// Definimos el tipo para el estado de los posts
export interface CompanyAttrs {
  id?: string;
  name: string;
  nit: number;
  phone: string;
  address: string;
}


// Definimos el tipo para el estado de los posts
export interface itemCompanyAttrs {
  id?: string;
  name: string;
  description: string;
  price: string;
  idCompany?: string;
  [others: string]: any;
}

// Definimos el tipo para el estado de los posts
export interface CompanyStateAttrs {
  companies: CompanyAttrs[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: string | null;
  companyId?: '' | undefined | null,
}

// Definimos el tipo para el estado de los posts
export interface itemCompanyStateAttrs {
  itemsCompany: itemCompanyAttrs[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: string | null;
  companyId?: '' | undefined | null,
}