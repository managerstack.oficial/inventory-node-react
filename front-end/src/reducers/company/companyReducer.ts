import {createSlice, current, PayloadAction } from '@reduxjs/toolkit';
import { CompanyStateAttrs } from '../../utils/interfaces/company/companyInterfaces';
import { companyList, companyCreate } from './companyServices';


// Slice to companySlice
const companySlice = createSlice({
  name: 'company',
  initialState: {
    companies: [],
    companyId: undefined,
    status: 'idle',
    error: null,
  } as unknown as CompanyStateAttrs,
  reducers: {
    setSelectedCompany: (state, action: PayloadAction<any>) => {
      state.companyId = action.payload;
    },
    setResetCompany: (state) => {
      state.companies = []
      state.companyId = undefined
      state.status = 'idle'
      state.error = null
    }
  },
  extraReducers: (builder) => {

    builder
    .addCase(companyList.pending, companyPending)
    .addCase(companyList.fulfilled, companyFulFilled)
    .addCase(companyList.rejected, companyRejected)
  }
});



const companyPending = (state: any) => {
  state.companies = [];
  state.status = 'loading';
  state.error = undefined;
}

const companyFulFilled = (state: any, action: any) => {
  state.companies = action.payload;
  state.status = 'succeeded';
  state.error = undefined;
}

const companyRejected = (state: any, action: any) => {
  state.companies = [];
  state.status = 'failed';
  state.error = action.payload?.message;
}

export const { setSelectedCompany, setResetCompany } = companySlice.actions;
export default companySlice.reducer;